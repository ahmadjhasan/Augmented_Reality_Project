This is a small application that

 -> Uses a webcam to track the movement of a camera relative to a recognized model (a checkerboard pattern in this particular case) in 3D space.
 
 -> Renders a 3D object (a Gaussian Manifold) on top of the recognized image and scales it based on the orientation.
 
 -> Uses a Linear Kalman Filter to filter the poses so that bad poses are rejected.

Usage: 

       cd build

       cmake ..
       
       make
       
       ./augmentedReality
       

Link to video: https://www.youtube.com/watch?v=DtMQFdzlWvE
