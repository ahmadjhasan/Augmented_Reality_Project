/*************************************************************
 * Author: Ahmad Hasan
 * Date: 11/23/2016
 * 
 * This is a program to generated to develop an application that-
 * 1. uses a webcam which tracks the movement of a camera relative to a recognized model (a checkerboard pattern in this particular case) *in 3D  space.
 * 
 * 2. Renders a 3D object (a Gaussian Manifold) on top of the recognized image and scale it based on the orientation.
 * 
 * 3. Uses a Linear Kalman Filter to filter the poses so that bad poses are rejected. 
 * **********************************************************/

#include <stdio.h>
#include <string>
#include<iostream>
#include<cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video/tracking.hpp>


using namespace cv;
using namespace std;

int boardHeight = 6;
int boardWidth = 9;
Size checkerBoardSize = Size(boardHeight,boardWidth);

string filename = "out_camera_data_1.xml";

bool complete = false;

//preset image size
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;

// Kalman Filter parameters
int minInliersKalman = 30;    // Kalman threshold updating

/**  Functions headers for implementation of Kalman Filter. Functions taken directly from openCV's Kalman Filter Implementation **/
void initKalmanFilter( KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt);
void updateKalmanFilter( KalmanFilter &KF, Mat &measurements,
			 Mat &translation_estimated, Mat &rotation_estimated );
void fillMeasurements( Mat &measurements,
		       const Mat &translation_measured, const Mat &rotation_measured);
cv::Mat rot2euler(const cv::Mat & rotationMatrix);
cv::Mat euler2rot(const cv::Mat & euler);

int main()
{	
	FileStorage fs;
	fs.open(filename, FileStorage::READ);
	
	Mat intrinsics, distortion;
	//Read intrinsic calibration Information
	
	fs["camera_matrix"] >> intrinsics;
	fs["distortion_coefficients"] >> distortion;
	fs.release();

	
	Mat srcImage, gray;
	Mat rvec = Mat(Size(3,1), CV_64F);
	Mat tvec = Mat(Size(3,1), CV_64F);

	//storage for 3D and 2D points
	vector<Point2d> imgPts, imgFramePts,coorSysPt2s;
	vector<Point3d> objPoints,coorSysPt3s;
	
	
	KalmanFilter KF;             // instantiate Kalman Filter
	int nStates = 18;            // the number of states
	int nMeasurements = 6;       // the number of measured states
	int nInputs = 0;             // the number of control actions
	double dt = 0.125;           // time between measurements (1/FPS)
	
	// initiate Kalman Filter
	initKalmanFilter(KF, nStates, nMeasurements, nInputs, dt);    
	Mat measurements(nMeasurements, 1, CV_64F); measurements.setTo(Scalar(0));

	//Set checker board based coordinate system
	for (int i=0; i<boardWidth; i++)
	{
		for (int j=0; j<boardHeight; j++)
		{
			objPoints.push_back( Point3d( double(i), double(j), 0.0) );
		}
	}
	
	// will be used to draw the coordinate axes
	coorSysPt3s.push_back( Point3d( 0.0, 0.0, 0.0 ) );
	coorSysPt3s.push_back( Point3d( 7.0, 0.0, 0.0 ) );
	coorSysPt3s.push_back( Point3d( 0.0, 5.0, 0.0 ) );
	coorSysPt3s.push_back( Point3d( 0.0, 0.0, 5.0 ) );
	
	//Start capturing video
	VideoCapture capture;
	capture.open(0);
	if(!capture.isOpened()){
		cout << "Error opening video stream" << endl;
		return -1;
	}
	
	//to write video
	VideoWriter video("out.avi",CV_FOURCC('M','J','P','G'),10, Size(FRAME_WIDTH,FRAME_HEIGHT),true);
	
	capture.set(CV_CAP_PROP_FRAME_WIDTH,FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT,FRAME_HEIGHT);
	
	//creating a Gaussian Manifold
	float sigma=0.8,ux=4,uy=2.5;
	vector<Point3d>spPt3s;
	for(float i= 0; i<8; i+=.03)
	{
		for(float j= 0; j<5; j+=0.03)
		
			{
				double x = i;
				double y = j;
				double z = 10*(1/(2*M_PI*sigma*sigma))*pow(2.718,-((x-ux)*(x-ux)+(y-uy)*(y-uy))/(2*sigma*sigma));//multiplied by 10 to increase the height while keeping the spread big
						
				Point3d spPt3(x,y,z);
				spPt3s.push_back(spPt3);
			}
	}
	
	while(!complete)
	{
		 capture.read(srcImage);
		 
		 cvtColor(srcImage,gray,COLOR_BGR2GRAY);
		 
		 //check if the checker board is found or not
		 bool seen = findChessboardCorners(gray, checkerBoardSize, imgPts, CALIB_CB_FAST_CHECK);

		 if ( seen )
		 {
			 vector<Point2d>spPt2s;
			 
			 //project 3D points to image
		         projectPoints(spPt3s, rvec, tvec, intrinsics, distortion, spPt2s );
			 projectPoints(coorSysPt3s, rvec, tvec, intrinsics, distortion, coorSysPt2s );
			
			//draw coordinate axes
			 line(srcImage, coorSysPt2s[0], coorSysPt2s[1], CV_RGB(0,0,255), 8 );
			 line(srcImage, coorSysPt2s[0], coorSysPt2s[2], CV_RGB(0,0,255), 8 );
			 line(srcImage, coorSysPt2s[0], coorSysPt2s[3], CV_RGB(0,0,255), 8 );
			 
			 //find the camera pose using Perspective n Point algorithm
			 solvePnP( Mat(objPoints), Mat(imgPts), intrinsics, distortion, rvec, tvec, false );
			 
			 Mat translation_measured(3,1,CV_64F);
			 translation_measured = tvec;
			 
			 Mat rotation_measured(3,3, CV_64F);
			 Rodrigues(rvec,rotation_measured);
			 
			 fillMeasurements(measurements, translation_measured, rotation_measured);
			 
			 Mat translation_estimated(3, 1, CV_64F);
			 Mat rotation_estimated(3, 3, CV_64F);
			 
			 //update state of Kalman Filter to get pose estimates
			 updateKalmanFilter( KF, measurements,translation_estimated, rotation_estimated);
			 
			 Mat rvec_est = Mat(Size(3,1), CV_64F); 
			 Rodrigues(rotation_estimated,rvec_est);
			 
			 //draw the gaussian manifold 
			 for(int i =0; i <=spPt2s.size();i++)
			 {
					circle(srcImage, spPt2s[i], 1 ,CV_RGB(255,0,0) );
			 }
			
		 }
		
		 namedWindow("AR", 0);
		 //visualize captured image stream
		 imshow("AR", srcImage);
		 //write the output AR video
		 video.write(srcImage);

		 char c = (char)waitKey(10);
		 //press esc to exit
		 if( c == 27 ) break; 
	}

	return 0;
}


/**********************************************************************************************************/
void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{
	
	KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter
	
	setIdentity(KF.processNoiseCov, Scalar::all(1e-5));       // set process noise
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));   // set measurement noise
	setIdentity(KF.errorCovPost, Scalar::all(1));             // error covariance
	
	
	/** DYNAMIC MODEL **/
	
	//  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]
	
	// position
	KF.transitionMatrix.at<double>(0,3) = dt;
	KF.transitionMatrix.at<double>(1,4) = dt;
	KF.transitionMatrix.at<double>(2,5) = dt;
	KF.transitionMatrix.at<double>(3,6) = dt;
	KF.transitionMatrix.at<double>(4,7) = dt;
	KF.transitionMatrix.at<double>(5,8) = dt;
	KF.transitionMatrix.at<double>(0,6) = 0.5*pow(dt,2);
	KF.transitionMatrix.at<double>(1,7) = 0.5*pow(dt,2);
	KF.transitionMatrix.at<double>(2,8) = 0.5*pow(dt,2);
	
	// orientation
	KF.transitionMatrix.at<double>(9,12) = dt;
	KF.transitionMatrix.at<double>(10,13) = dt;
	KF.transitionMatrix.at<double>(11,14) = dt;
	KF.transitionMatrix.at<double>(12,15) = dt;
	KF.transitionMatrix.at<double>(13,16) = dt;
	KF.transitionMatrix.at<double>(14,17) = dt;
	KF.transitionMatrix.at<double>(9,15) = 0.5*pow(dt,2);
	KF.transitionMatrix.at<double>(10,16) = 0.5*pow(dt,2);
	KF.transitionMatrix.at<double>(11,17) = 0.5*pow(dt,2);
	
	
	/** MEASUREMENT MODEL **/
	
	//  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]
	
	KF.measurementMatrix.at<double>(0,0) = 1;  // x
	KF.measurementMatrix.at<double>(1,1) = 1;  // y
	KF.measurementMatrix.at<double>(2,2) = 1;  // z
	KF.measurementMatrix.at<double>(3,9) = 1;  // roll
	KF.measurementMatrix.at<double>(4,10) = 1; // pitch
	KF.measurementMatrix.at<double>(5,11) = 1; // yaw
	
}

/**********************************************************************************************************/
void updateKalmanFilter( KalmanFilter &KF, Mat &measurement,
			 Mat &translation_estimated, Mat &rotation_estimated )
{
	
	// First predict, to update the internal statePre variable
	Mat prediction = KF.predict();
	
	// The "correct" phase that is going to use the predicted value and our measurement
	Mat estimated = KF.correct(measurement);
	
	// Estimated translation
	translation_estimated.at<double>(0) = estimated.at<double>(0);
	translation_estimated.at<double>(1) = estimated.at<double>(1);
	translation_estimated.at<double>(2) = estimated.at<double>(2);
	
	// Estimated euler angles
	Mat eulers_estimated(3, 1, CV_64F);
	eulers_estimated.at<double>(0) = estimated.at<double>(9);
	eulers_estimated.at<double>(1) = estimated.at<double>(10);
	eulers_estimated.at<double>(2) = estimated.at<double>(11);
	
	// Convert estimated quaternion to rotation matrix
	rotation_estimated = euler2rot(eulers_estimated);
	
}

/**********************************************************************************************************/
void fillMeasurements( Mat &measurements,
		       const Mat &translation_measured, const Mat &rotation_measured)
{
	// Convert rotation matrix to euler angles
	Mat measured_eulers(3, 1, CV_64F);
	measured_eulers = rot2euler(rotation_measured);
	
	// Set measurement to predict
	measurements.at<double>(0) = translation_measured.at<double>(0); // x
	measurements.at<double>(1) = translation_measured.at<double>(1); // y
	measurements.at<double>(2) = translation_measured.at<double>(2); // z
	measurements.at<double>(3) = measured_eulers.at<double>(0);      // roll
	measurements.at<double>(4) = measured_eulers.at<double>(1);      // pitch
	measurements.at<double>(5) = measured_eulers.at<double>(2);      // yaw
}

// Converts a given Rotation Matrix to Euler angles
cv::Mat rot2euler(const cv::Mat & rotationMatrix)
{
	cv::Mat euler(3,1,CV_64F);
	
	double m00 = rotationMatrix.at<double>(0,0);
	double m02 = rotationMatrix.at<double>(0,2);
	double m10 = rotationMatrix.at<double>(1,0);
	double m11 = rotationMatrix.at<double>(1,1);
	double m12 = rotationMatrix.at<double>(1,2);
	double m20 = rotationMatrix.at<double>(2,0);
	double m22 = rotationMatrix.at<double>(2,2);
	
	double x, y, z;
	
	// Assuming the angles are in radians.
	if (m10 > 0.998) { // singularity at north pole
		x = 0;
		y = CV_PI/2;
		z = atan2(m02,m22);
	}
	else if (m10 < -0.998) { // singularity at south pole
		x = 0;
		y = -CV_PI/2;
		z = atan2(m02,m22);
	}
	else
	{
		x = atan2(-m12,m11);
		y = asin(m10);
		z = atan2(-m20,m00);
	}
	
	euler.at<double>(0) = x;
	euler.at<double>(1) = y;
	euler.at<double>(2) = z;
	
	return euler;
}

// Converts a given Euler angles to Rotation Matrix
cv::Mat euler2rot(const cv::Mat & euler)
{
	cv::Mat rotationMatrix(3,3,CV_64F);
	
	double x = euler.at<double>(0);
	double y = euler.at<double>(1);
	double z = euler.at<double>(2);
	
	// Assuming the angles are in radians.
	double ch = cos(z);
	double sh = sin(z);
	double ca = cos(y);
	double sa = sin(y);
	double cb = cos(x);
	double sb = sin(x);
	
	double m00, m01, m02, m10, m11, m12, m20, m21, m22;
	
	m00 = ch * ca;
	m01 = sh*sb - ch*sa*cb;
	m02 = ch*sa*sb + sh*cb;
	m10 = sa;
	m11 = ca*cb;
	m12 = -ca*sb;
	m20 = -sh*ca;
	m21 = sh*sa*cb + ch*sb;
	m22 = -sh*sa*sb + ch*cb;
	
	rotationMatrix.at<double>(0,0) = m00;
	rotationMatrix.at<double>(0,1) = m01;
	rotationMatrix.at<double>(0,2) = m02;
	rotationMatrix.at<double>(1,0) = m10;
	rotationMatrix.at<double>(1,1) = m11;
	rotationMatrix.at<double>(1,2) = m12;
	rotationMatrix.at<double>(2,0) = m20;
	rotationMatrix.at<double>(2,1) = m21;
	rotationMatrix.at<double>(2,2) = m22;
	
	return rotationMatrix;
}
